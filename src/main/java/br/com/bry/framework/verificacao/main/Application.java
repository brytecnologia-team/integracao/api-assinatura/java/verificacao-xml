package br.com.bry.framework.verificacao.main;

import br.com.bry.framework.verificacao.VerifierXML;

public class Application {

	public static void main(String[] args) {

		VerifierXML xmlVerification = new VerifierXML();

		System.out.println(
				"=====================================Verification of XML Signature:=====================================");

		xmlVerification.verifyXMLSignature();

	}

}
