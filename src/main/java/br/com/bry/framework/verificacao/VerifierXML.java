package br.com.bry.framework.verificacao;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bry.framework.verificacao.config.ServiceConfig;
import br.com.bry.framework.verificacao.config.VerifierConfig;
import br.com.bry.framework.verificacao.util.ConverterUtil;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class VerifierXML {

	private String token = ServiceConfig.ACCESS_TOKEN;

	private static final String URL_XML_SERVER = ServiceConfig.URL_XML_VERIFIER;

	private ConverterUtil converterUtil = new ConverterUtil();

	public void verifyXMLSignature() {

		if (this.token.equals("<INSERT_VALID_ACCESS_TOKEN>")) {
			System.out.println("Set up a valid token!");
			return;
		}

		try {
			Object verifierResponse = null;
			String filePathOfSignature = VerifierConfig.SIGNATURE_PATH;

			RequestSpecBuilder builder = new RequestSpecBuilder();

			int signatureQuantitiesToVerify = 1;

			for (int indexOfSignature = 0; indexOfSignature < signatureQuantitiesToVerify; indexOfSignature++) {
				builder.addMultiPart("signatures[" + indexOfSignature + "][nonce]", Integer.toString(indexOfSignature));
				builder.addMultiPart("signatures[" + indexOfSignature + "][content]", new File(filePathOfSignature));
			}

			RequestSpecification requestSpec = builder.build();

			verifierResponse = RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec)
					.multiPart("nonce", VerifierConfig.NONCE)
					.multiPart("contentsReturn", VerifierConfig.CONTENTS_RETURN).expect().statusCode(200).when()
					.post(URL_XML_SERVER).as(Object.class);

			String jsonResponse = this.converterUtil.convertObjectToJSON(verifierResponse);

			System.out.println(jsonResponse);

			JSONObject jsonObject = new JSONObject(jsonResponse);

			JSONArray verificationStatus = jsonObject.getJSONArray("verificationStatus");

			String verificationStatusStringJson = verificationStatus.getString(0);

			JSONObject verificationStatusjsonObject = new JSONObject(verificationStatusStringJson);

			int nonceOfverificationStatus = verificationStatusjsonObject.getInt("nonce");

			String content = verificationStatusjsonObject.getString("generalStatus");

			System.out.println("Nonce of verified signature: " + nonceOfverificationStatus);

			System.out.println("Verification status: " + content);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
