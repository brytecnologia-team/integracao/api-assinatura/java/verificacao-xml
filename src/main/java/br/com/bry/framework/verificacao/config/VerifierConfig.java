package br.com.bry.framework.verificacao.config;

import java.io.File;

public class VerifierConfig {

	private static final String PATH_SEPARATOR = File.separator;

	// Request identifier
	public static final String NONCE = "1";

	// Available values: 'true' = Report with signature timestamp information and
	// 'false' = Report without signature timestamp information
	public static final boolean CONTENTS_RETURN = true;

	// location where the signature is stored
	public static final String SIGNATURE_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "assinaturas" + PATH_SEPARATOR
			+ "assinaturaXAdESADRBEnveloped-teste.xml";
}
